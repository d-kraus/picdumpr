#[macro_use]
extern crate log;
extern crate env_logger;

extern crate chrono;
extern crate reqwest;

use chrono::prelude::*;
use std::collections::HashMap;

use reqwest::StatusCode;
use std::{env, process, thread, time};

fn main() {
    env_logger::init();
    info!("loading env vars");
    let bot_api_key: String = fetch_env_var("API_KEY");
    let chat_id: String = fetch_env_var("CHAT_ID");

    info!("Starting Picdump crawl");
    let backoff = time::Duration::from_secs(30);
    let url: String = generate_picdump_url();
    info!("Checking Url {:?}", url);

    while !picdump_exists(url.as_str()) {
        info!("No picdump yet, sleeping");
        thread::sleep(backoff);
    }

    info!("New Picdump found, posting ...");

    match post_to_channel(chat_id, bot_api_key) {
        Ok(resp) => {
            info!("Posting response: {}", resp.status());
            info!("Crawl finished, enjoy");
        }
        Err(e) => {
            info!("Error response: {}", e.status().unwrap());
            info!("Crawl aborted");
        }
    }
}

fn post_to_channel(
    chat_id: String,
    bot_api_key: String,
) -> Result<reqwest::Response, reqwest::Error> {
    let endpoint_url = format!("https://api.telegram.org/bot{}/sendMessage", bot_api_key);
    let chat_msg: String = format!(
        "Kinnas, der neue Picdump ist da\n{}",
        generate_picdump_url()
    );

    let mut payload = HashMap::new();
    payload.insert("chat_id", chat_id);
    payload.insert("text", chat_msg);

    let client = reqwest::Client::new();
    client.post(endpoint_url.as_str()).json(&payload).send()
}

fn generate_picdump_url() -> String {
    let today: Date<Local> = Local::today();
    format!(
        "http://www.bildschirmarbeiter.com/pic/bildschirmarbeiter_-_picdump_kw_{:02}_{}/",
        today.iso_week().week(),
        today.year()
    )
}

fn fetch_env_var(key: &str) -> String {
    match env::var(key) {
        Ok(val) => val,
        Err(_) => {
            info!("ENV {} not found, aborting", key);
            process::exit(1);
        }
    }
}

fn picdump_exists(url: &str) -> bool {
    reqwest::get(url)
        .map(|resp| resp.status() == StatusCode::OK)
        .unwrap_or(false)
}

#[cfg(test)]
mod tests {
    use super::{generate_picdump_url, picdump_exists};
    use chrono::prelude::*;
    #[test]
    fn test_picdump_not_exists() {
        assert_eq!(
            picdump_exists(
                "http://www.bildschirmarbeiter.com/pic/bildschirmarbeiter_-_picdump_kw_01_2020/"
            ),
            false
        );
    }

    #[test]
    fn test_picdump_exists() {
        assert_eq!(
            picdump_exists(
                "http://www.bildschirmarbeiter.com/pic/bildschirmarbeiter_-_picdump_kw31_2020/"
            ),
            true
        );
    }

    #[test]
    fn test_generate_picdump_url() {

    let today: Date<Local> = Local::today();
    let expected = format!("_kw_{:02}_{}/",
        today.iso_week().week(),
        today.year()
    );
        assert!(generate_picdump_url().ends_with(expected.as_str()));
    }
}
