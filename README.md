# PicdumpeR - A telegram picdump bot in Rust

Checks periodically if today's pcidump exists and posts it to Telegram. 

## Running the bot
You need an Telegram bot api key and the ID of the group to post to.

It expects 2 environment variables to be set `API_KEY` and `CHAT_ID`.
```
$ API_KEY=foobar CHAT_ID=bar picdumpr
```

Logging can be set via the `RUST_LOG` environment variable, see [env_logger][env_logger] for details
```
$ RUST_LOG=info API_KEY=foo CHAT_ID=bar picdumpr
```


[env_logger]: https://docs.rs/env_logger/0.6.2/env_logger/